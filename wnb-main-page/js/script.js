'use strict'

if (document.getElementsByTagName('html')[0].classList.contains('desktop')){
	(function(){
		 
		var BREAK_POINT = 850,
		INDENTION = '17px',
		container = document.getElementsByClassName('container')[0],
		wrapper = document.getElementsByClassName('wrapper')[0],
		aside = document.getElementsByTagName('aside')[0],
		header = document.getElementsByTagName( 'header' )[0],
		burger = document.getElementById('burgerCheckbox');
		
		window.addEventListener('resize', doubleScrollPrevent );
		burger.addEventListener( 'change', function(){
			if ( contentHasScroll() ){
				if( burger.checked ){
					header.style.right = '';
				} else {
					header.style.right = INDENTION;
				}
			}
		});

		doubleScrollPrevent();

		function necessaryDimensions(){
			return window.innerWidth > BREAK_POINT && window.innerHeight < aside.clientHeight;
		}

		function hideContainerScroll(event){
				wrapper.style.overflow = '';
				container.style.overflow = 'hidden';
				defineHeaderRightPosition();
		}

		function hideAsideScroll(event){
				wrapper.style.overflow = 'hidden';
				container.style.overflow = '';
				defineHeaderRightPosition();
		}

		function defineHeaderRightPosition(){
				if (container.style.overflow === 'hidden' || (window.innerWidth < BREAK_POINT && burger.checked) ){
					header.style.right = '';
				} else {
					header.style.right = INDENTION;	
				}
		}

		function doubleScrollPrevent(){
			if ( necessaryDimensions() ){
				aside.style.maxWidth = '300px';
				header.style.left = '300px';

				if( contentHasScroll() ){
					aside.addEventListener( 'mouseover', hideContainerScroll );
					container.addEventListener( 'mouseover', hideAsideScroll );
					hideAsideScroll();
				}
			} else {
				reset();
				if ( contentHasScroll() ){
					defineHeaderRightPosition();
				}
			}
		}

		function contentHasScroll(){
			return container.scrollHeight > window.innerHeight;
		}

		function reset(){
			aside.removeEventListener( 'mouseover', hideContainerScroll );
			container.removeEventListener( 'mouseover', hideAsideScroll );
			wrapper.style.overflow = '';
			container.style.overflow = '';
			header.style.left = '';
			aside.style.maxWidth = '';
		}

		window.doubleScrollPrevent = doubleScrollPrevent;

	})();
}

